// $Id$

Drupal.behaviors.evenBetterSelect = function(context) {
  // Add in UL to act as tab handles
  $(".better-select .form-checkboxes").before('<ul class="even-better-select-tabs"></ul>');
  
  // Are we scrolling?
  var hasScroll = false;
  $('.better-select .form-checkboxes-scroll').each(function(){
    $(this).removeClass('form-checkboxes-scroll');
    hasScroll = true;
  });
  
  // Rearrange elements by tab
  $('.even-better-select-root').each(function(){
    var id = $(this).attr('id');
    var classes = $(this).attr('class').split(' ');
    var wrapper = $('#' + id + '-wrapper');
    
    // Find the tab class
    for (i in classes) {
      var matched = classes[i].match(/even-better-select-tab-\d+/);
      if (matched) {
        // Setup tab info
        $('.even-better-select-tabs').append('<li><a href="#' + id + '-wrapper">' + jQuery.trim($(this).parent().text()) + '</a></li>');
        $('#' + id + '-wrapper').addClass('tab');
        
        if (hasScroll) {
          wrapper.append('<div class="children form-checkboxes-scroll"></div>');
        }
        else {
          wrapper.append('<div class="children"></div>');
        }
        
        // Move child elements into the tab wrapper element
        $('div.form-checkboxes div.form-item:has(.' + matched + ':not(.even-better-select-root))').appendTo(wrapper.children('.children'));
      }
    }
  });
  
  // Rearrange elements by tab group
  $('.tab:has(.even-better-select-root)').each(function(){
    var tab = this
    $('.even-better-select-group-root', tab).each(function(){
      var id = $(this).attr('id');
      var classes = $(this).attr('class').split(' ');
      var wrapper = $('#' + id + '-wrapper');
      
      // Add classes to the wrapper element
      wrapper.addClass('even-better-select-group-root-wrapper');
  
      // Find the tab class
      for (i in classes) {
        var matched = classes[i].match(/even-better-select-group-\d+/);
        if (matched) {
          // Move child elements into the tab wrapper element
          $('div.form-item:has(.' + matched + ':not(.even-better-select-group-root))', tab).appendTo(wrapper);
        }
      }
    });
  });
  
  // Initialize tabs handler
  $(".even-better-select-tabs").parents('.form-item:first').tabs();
}
